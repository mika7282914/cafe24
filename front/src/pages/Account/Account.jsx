import './Account.css';
import List from '../../components/List/List';
import TtileArea from '../../components/TitleArea/TtileArea';

const Account = () => {
    return (
        <div className="account container">
            <TtileArea
                Title="계정 등록"
                DeleteTxt="계정 삭제"
                DeleteType="delete"
                EditType="edit"
                EditTxt="계정 편집"
                DefaultType="default"
                DefaultTxt="신규 계정 등록"
            />
            <List />
        </div>
    );
};

export default Account;
