import { Route, Routes } from 'react-router-dom';
import './css/reset.css';
import './css/common.css';
import Account from './pages/Account/Account';
import Aside from './components/Aside/Aside';
import Header from './components/Header/Header';

function App() {
    return (
        <div className="App">
            <Aside />
            <div className="mainWrap">
                <Header />
                <Routes>
                    <Route path="/account" element={<Account />} />
                </Routes>
            </div>
        </div>
    );
}

export default App;
