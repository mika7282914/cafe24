import './TitleArea.css';
import UpdateBtn from './UpdateBtn/UpdateBtn';

const TtileArea = ({ Title, DeleteType, DeleteTxt, EditType, EditTxt, DefaultType, DefaultTxt }) => {
    return (
        <div className="titleArea">
            <h2 className="title">{Title}</h2>
            <UpdateBtn DeleteType={DeleteType} DeleteTxt={DeleteTxt} EditType={EditType} EditTxt={EditTxt} DefaultType={DefaultType} DefaultTxt={DefaultTxt} />
        </div>
    );
};

export default TtileArea;
