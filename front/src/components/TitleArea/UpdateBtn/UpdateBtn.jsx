import BtnTypeA from '../../Btn/BtnTypeA';
import Search from '../../Search/Search';

export default function UpdateBtn({ DeleteType, DeleteTxt, EditType, EditTxt, DefaultType, DefaultTxt }) {
    return (
        <>
            <div className="sub">
                <BtnTypeA Type={DeleteType} Text={DeleteTxt} />
                <BtnTypeA Type={EditType} Text={EditTxt} />
                <Search />
                <BtnTypeA Type={DefaultType} Text={DefaultTxt} />
            </div>
        </>
    );
}
