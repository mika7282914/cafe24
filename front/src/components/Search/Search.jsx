import './Search.css';

const Search = () => {
    return (
        <div className="search box">
            <p>등록할 채널을 선택해 주세요.</p>
            <div className="icon"></div>
        </div>
    );
};

export default Search;
