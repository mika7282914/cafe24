import './Aside.css';

const Aside = () => {
    return (
        <div className="aside">
            <h1 className="logo">
                <a href="#">
                    <img src="https://milvus.co.kr/image/cafe24/logo_sf.png" alt="" />
                    <span>데이터 중계서버 솔루션</span>
                </a>
            </h1>
            <ul className="tab">
                <li className="active">
                    <div className="icon">
                        <img src="https://milvus.co.kr/image/cafe24/icon_account_active.png" alt="" />
                    </div>
                    계정 관리
                </li>
                <li>
                    <div className="icon">
                        <img src="https://milvus.co.kr/image/cafe24/icon_data.png" alt="" />
                    </div>
                    데이터 스트림
                </li>
                <li>
                    <div className="icon">
                        <img src="https://milvus.co.kr/image/cafe24/icon_log.png" alt="" />
                    </div>
                    로그 조회
                </li>
            </ul>
        </div>
    );
};

export default Aside;
