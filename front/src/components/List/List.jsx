import './List.css';
import AccountList from '../../assets/json/AccountList.json';

const List = () => {
    const accountList = AccountList.account;
    return (
        <div className="listArea">
            <ul className="list">
                <li className="column">
                    <div className="checkBox"></div>
                    <div className="type">Type</div>
                    <div className="name">Name</div>
                    <div className="creator">Creator</div>
                    <div className="createDate">
                        Create Date
                        <div className="icon"></div>
                    </div>
                    <div className="updatedDate">
                        Updated Date
                        <div className="icon"></div>
                    </div>
                    <div className="status">
                        Status<div className="icon"></div>
                    </div>
                </li>
                {accountList.slice(0, 10).map((account, i) => {
                    return (
                        <li className="" key={account.id}>
                            <div className="checkBox"></div>
                            <div className={`type ${account.type}`}></div>
                            <div className="name">{account.name}</div>
                            <div className="creator">{account.creator}</div>
                            <div className="createDate">{account.createDate}</div>
                            <div className="updatedDate">{account.updatedDate}</div>
                            <div className="status">
                                <div className={`box ${account.status}`}>{account.status}</div>
                            </div>
                        </li>
                    );
                })}
            </ul>
            <div className="pasingArea">
                <div className="prev box"></div>
                <div className="pasingNum">1</div>
                <div className="next box"></div>
            </div>
        </div>
    );
};

export default List;
