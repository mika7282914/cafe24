import './BtnTypeA.css';

const Btn = ({ Type, Text }) => {
    return (
        <div className={`btn box ${Type}`}>
            <div className="icon"></div>
            <span>{Text}</span>
        </div>
    );
};

export default Btn;
