import '../Header/Header.css';
import Select from '../Select/Select';

const Header = () => {
    return (
        <div className="header">
            <Select />
        </div>
    );
};

export default Header;
